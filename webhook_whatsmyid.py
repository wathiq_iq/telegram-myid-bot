#!/usr/bin/python3.4
# -*- coding: utf-8 -*-

import cherrypy
import telebot
from telebot import types
import botan


API_TOKEN = 'Telegram_bot_api_token'
BOTAN_TOKEN = "Botan_token"

# Not using certificate, because this bot is behind other CherryPy server or nginx
WEBHOOK_PORT = 1234 
WEBHOOK_LISTEN = '0.0.0.0'


bot = telebot.TeleBot(API_TOKEN)


# WebhookServer, process webhook calls
class WebhookServer(object):
    @cherrypy.expose
    def index(self):
        if 'content-length' in cherrypy.request.headers and \
           'content-type' in cherrypy.request.headers and \
           cherrypy.request.headers['content-type'] == 'application/json':
            length = int(cherrypy.request.headers['content-length'])
            json_string = cherrypy.request.body.read(length)
            json_string = json_string.decode("utf-8")
            update = telebot.types.Update.de_json(json_string)
            if update.message:
                bot.process_new_messages([update.message])
            if update.inline_query:
                bot.process_new_inline_query([update.inline_query])
            return ''
        else:
            raise cherrypy.HTTPError(403)
            
            
@bot.message_handler(commands=["start"])
def cmd_start(message):
    if message.chat.type == 'private':
        bot.send_message(message.chat.id, 'Your personal ID is *{0!s}*\nNever tell it to whom you don\'t trust!\nYou can also add me to any group to know its ID or send me any type of media to get its `file_id`\n\nBy the way, I\'m open source! Check [my bitbucket page](https://bitbucket.org/master_groosha/telegram-myid-bot/src) for details.'.format(message.chat.id), parse_mode='Markdown') 
        botan.track(BOTAN_TOKEN, message.chat.id, None, "ID request (private chat)")
    elif message.chat.type == "group" or message.chat.type == "supergroup":
        bot.reply_to(message, 'This group\'s ID is *{0!s}*\nTo view your personal ID, please, open a separate chat with me.\n\nBy the way, I\'m open source! Check [my bitbucket page](https://bitbucket.org/master_groosha/telegram-myid-bot/src) for details.'.format(message.chat.id), parse_mode='Markdown') 
        botan.track(BOTAN_TOKEN, message.chat.id, None, "ID request (group chat)")

            
@bot.message_handler(content_types = ['text'])
def parse_text(message):
    # Send chat_id if private chat
    if message.chat.type == 'private':
        bot.send_message(message.chat.id, 'Your personal ID is *{0!s}*\nNever tell it to whom you don\'t trust!\nYou can also add me to any group to know its ID or send me any type of media to get its `file_id`\n\nBy the way, I\'m open source! Check [my bitbucket page](https://bitbucket.org/master_groosha/telegram-myid-bot/src) for details.'.format(message.chat.id), parse_mode='Markdown') 
        botan.track(BOTAN_TOKEN, message.chat.id, None, "ID request (private chat)")
    # Send both chat_id and user's own id if group chat
    elif message.chat.type == "group" or message.chat.type == "supergroup":
        bot.reply_to(message, 'This group\'s ID is *{0!s}*\nTo view your personal ID, please, open a separate chat with me.\n\nBy the way, I\'m open source! Check [my bitbucket page](https://bitbucket.org/master_groosha/telegram-myid-bot/src) for details.'.format(message.chat.id), parse_mode='Markdown') 
        botan.track(BOTAN_TOKEN, message.chat.id, None, "ID request (group chat)")
    
    
@bot.message_handler(content_types = ['sticker'])
def send_sticker_id(message):
    bot.reply_to(message, 'This sticker ID is:\n{0!s}'.format(message.sticker.file_id))
    botan.track(BOTAN_TOKEN, message.chat.id, None, "sticker file_id request")
    

@bot.message_handler(content_types = ['photo'])
# Send file_id of largest copy of incoming photo
def send_sticker_id(message):
    bot.reply_to(message, 'This photo ID is:\n{0!s}'.format(message.photo[-1].file_id))
    botan.track(BOTAN_TOKEN, message.chat.id, None, "photo file_id request")
    

@bot.message_handler(content_types = ['audio'])
def send_sticker_id(message):
    bot.reply_to(message, 'This audio ID is:\n{0!s}'.format(message.audio.file_id))
    botan.track(BOTAN_TOKEN, message.chat.id, None, "audio file_id request")
    
    
@bot.message_handler(content_types = ['video'])
def send_sticker_id(message):
    bot.reply_to(message, 'This video ID is:\n{0!s}'.format(message.video.file_id))
    botan.track(BOTAN_TOKEN, message.chat.id, None, "video file_id request")
    
 
@bot.message_handler(content_types = ['document'])
def send_document_id(message):
    bot.reply_to(message, 'This document ID is:\n{0!s}'.format(message.document.file_id))
    botan.track(BOTAN_TOKEN, message.chat.id, None, "document file_id request")
    
    
@bot.message_handler(content_types = ['voice'])
def send_voice_id(message):
    bot.reply_to(message, 'This voice ID is:\n{0!s}'.format(message.voice.file_id))
    botan.track(BOTAN_TOKEN, message.chat.id, None, "voice file_id request")
    
    
@bot.inline_handler(lambda query: True)
def query_zero(query):
    try:
        result = types.InlineQueryResultArticle(
            id='1',
            title="Your ID is {!s}".format(query.from_user.id),
            description="Click to send your ID to current chat.",
            message_text="My personal ID is *{!s}*".format(query.from_user.id),
            parse_mode="Markdown",
            thumb_url="https://pp.vk.me/c629419/v629419512/2b8a4/ePHZEaeRGbU.jpg",
            thumb_width=64,
            thumb_height=64
        )
        # Cache for everyone personally for 10 days
        bot.answer_inline_query(query.id, [result], cache_time=864000, is_personal=True)
        botan.track(BOTAN_TOKEN, 0, None, "ID request (inline mode)")
    except Exception as e:
        print(e)
    
            
if __name__ == '__main__':
   # Start cherrypy server
    cherrypy.config.update({
    'server.socket_host': WEBHOOK_LISTEN,
    'server.socket_port': WEBHOOK_PORT
    })

    cherrypy.quickstart(WebhookServer(), '/', {'/': {}})