# -*- coding: utf-8 -*-
# Modified for pyTelegramBotAPI (https://github.com/eternnoir/pyTelegramBotAPI/)

import requests
import json
import telebot
from telebot import types

URL_TEMPLATE = 'https://api.botan.io/track?token={token}&uid={uid}&name={name}'
SHORTENER_URL = 'https://api.botan.io/s/'

'''
def make_json(message):
    data = {}
    data['from'] = {}
    data['from']['id'] = message.from_user.id
    if message.from_user.username is not None:
        data['from']['username'] = message.from_user.username
    data['chat'] = {}
    # Chat.Id is in both User and GroupChat
    data['chat']['id'] = message.chat.id
    return data
'''

def track(token, uid, msg, name = 'Message'):
    global url_template
    url = URL_TEMPLATE.format(token=str(token), uid=str(uid), name=name)
    headers = {'Content-type': 'application/json'}
    try:
        r = requests.post(url, data=json.dumps(msg), headers=headers)
        return r.json()
    except requests.exceptions.Timeout:
        # set up for a retry, or continue in a retry loop
        return False
    except requests.exceptions.RequestException as e:
        # catastrophic error
        print(e)
        return False
        
        
def shorten_url(url, botan_token, user_id):
    """
    Shorten URL for specified user of a bot
    """
    try:
        return requests.get(SHORTENER_URL, {
            'token': botan_token,
            'url': url,
            'user_ids': str(user_id),
        }).text
    except:
        return url