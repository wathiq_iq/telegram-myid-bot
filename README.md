## "What's my Telegram ID?" bot ##

Ask this bot in normal chat and it will tell your own chat_id
Add it to group or supergroup and it will tell group's chat_id.

Pretty simple, huh?

* Using [pyTelegramBotAPI](https://github.com/eternnoir/pyTelegramBotAPI/) to get bot working.  
* Using [botan](http://botan.io/) to collect analytics data.